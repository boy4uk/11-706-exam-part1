﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Diagnostics;
using MeSkypeProj.Models;

namespace MeSkypeProj.Controllers
{
    public class HomeController : Controller
    {
        IHostingEnvironment _appEnvironment;

        public HomeController(IHostingEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            return View(LocalDatabase.Files);
        }

        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                // путь к папке Files
                string path = "/Files/" + uploadedFile.FileName;
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
                MyFile file = new MyFile { Name = uploadedFile.FileName, Path = path };

                LocalDatabase.Files.Add(file);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public FileResult Download()
        {
            var s1 = Request.Form["filePath"];
            var s2 = Request.Form["fileName"];
            LocalDatabase.Files.Where(a => a.Name.Equals(s2)).First().DownloadsAmount++;
            byte[] fileBytes = System.IO.File.ReadAllBytes(_appEnvironment.WebRootPath + s1);
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = s2;
            return response;
        }

        public IActionResult Create()
        {
            return View(LocalDatabase.Users);
        }

        [HttpPost]
        public IActionResult Create(UserViewModel pvm)
        {
            User user = new User { Name = pvm.Name };
            if (pvm.Avatar != null)
            {
                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(pvm.Avatar.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)pvm.Avatar.Length);
                }
                user.Avatar = imageData;
            }
            LocalDatabase.Users.Add(user);

            return RedirectToAction("Create");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
