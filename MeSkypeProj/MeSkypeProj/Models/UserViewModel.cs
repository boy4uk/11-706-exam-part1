﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MeSkypeProj.Models
{
    public class UserViewModel
    {
        public string Name { get; set; }
        public IFormFile Avatar { get; set; }
    }
}
