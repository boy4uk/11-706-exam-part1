﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeSkypeProj.Models
{
    public static class LocalDatabase
    {
        /*  Сделал так только потому что вы попросили хранить файлы в оперативке.
         *  Уже загруженные файлы не будут отображаться после перезапуска.
         */

        public static List<MyFile> Files = new List<MyFile>();
        public static List<User> Users = new List<User>();
    }
}
